# AVVISAMELO

A small utility written in Rust to parse an RSS feed and send to email
items that contains specified terms.

## Need

- `sendmail` to send.. mails...
- `cargo` to compile


## Use

    USAGE:
        avvisamelo [OPTIONS] --from <from> --url <url> [--] [TERM]...

    FLAGS:
        -h, --help       Prints help information
        -V, --version    Prints version information

    OPTIONS:
        -c, --cache <cachefile>    Path to cache db (optional, defaults to "./itemcache.db")
        -f, --from <from>          Sender email address
        -t, --to <to>...           List of recipient addresses
        -l, --timetolive <ttl>     Items older than this ours will be ignored (optional, default: 48)
        -u, --url <url>            RSS Url to check

    ARGS:
        <TERM>...    Terms to search in RSS, case insensitive

the cache file keeps a list of sent GUID to not resend

## Example

    avvisamelo --from notifier@example.com --to me@example.com --timetolive 24 --url "http://traingroup.com/news.rss" "strike"
    
Send via mail to `me@example.com` all news posted in the last 24 ours from Train Group about a strike.

## Cross-compile
https://github.com/messense/rust-musl-cross
