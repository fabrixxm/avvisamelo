//#[macro_use]
extern crate structopt;
extern crate rss;
extern crate chrono;
extern crate sendmail;

use std::fs;
use std::io::ErrorKind;
use std::error::Error;
use rss::Channel;
use chrono::{DateTime, Local, Duration, FixedOffset };
use sendmail::email;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(raw(setting = "structopt::clap::AppSettings::ColoredHelp"))]
struct Opt {
    /// Sender email address
    #[structopt(short = "f", long = "from")]
    from: String,

    /// RSS Url to check
    #[structopt(short = "u", long = "url")]
    url: String,

    /// Terms to search in RSS, case insensitive
    #[structopt(name = "TERM")]
    terms: Vec<String>,

    /// Items older than this ours will be ignored (optional, default: 48)
    #[structopt(short = "l", long = "timetolive")]
    ttl: Option<i64>,

    /// List of recipient addresses
    #[structopt(short = "t", long = "to")]
    to: Vec<String>,

    /// Path to cache db (optional, defaults to "./itemcache.db")
    #[structopt(short = "c", long = "cache")]
    cachefile: Option<String>,
}

fn to_fixed(date_time: DateTime<Local>) -> DateTime<FixedOffset>
{
    date_time.with_timezone(date_time.offset())
}

fn main() -> Result<(), Box<dyn Error>> 
{

    let opt = Opt::from_args();

    // convert opt.to: Vec<String> to Vec<&str> aka [&str], to use it in email::send
    let _to: Vec<&str> = opt.to.iter().map(AsRef::as_ref).collect();

    let channel = Channel::from_url(&opt.url)?;

    let ttl = opt.ttl.unwrap_or(48);
    let lastrun = to_fixed( Local::now() - Duration::hours(ttl) );

    let cachefile = opt.cachefile.unwrap_or("./itemcache.db".to_string());

    // load cache of already sent messages
    
    let mut cache = match fs::read_to_string(&cachefile) {
        Ok(content) => content,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => "## avvisamelo sent cache".to_string(),
            _ => return Err(Box::from(error)),
        }
    };


    for item in channel.items() {
        // se l'item non ha la data di pubblicazione, che fare?
        // per ora lo mettiamo a "oggi - ttl".
        // altrimenti mi manda una mail ogni volta..
        let item_pubdate = match item.pub_date() {
            Some(strdate) => match DateTime::parse_from_rfc2822(strdate) {
                Ok(date) => date,
                Err(_) => to_fixed( Local::now() - Duration::hours(ttl) )
            },
            None => to_fixed( Local::now() - Duration::hours(ttl) ),
        };

        let title = match item.title() {
            Some(t) => format!("[{}] {}", channel.title(), t),
            None => String::from(channel.title()),
        };

        let content = item.content().unwrap_or("");

        let guid = match item.guid() {
            Some(guid) => guid.value(),
            None => item.link().or_else(|| item.title()).unwrap(), // `|| ...` it's a closure
        };

        // item must be newest than ttl, and must not be already in cache
        if item_pubdate > lastrun && !cache.contains(guid){
            for term in &opt.terms {
                if title.to_lowercase().find(&term.to_lowercase()).is_some() || content.to_lowercase().find(&term.to_lowercase()).is_some() {
                    println!("{}", title);
                    // send mail, if successfull, add item guid to cache
                    cache = email::send(
                        // From Address
                        &opt.from,
                        // To Address
                        &_to,
                        // Subject
                        &title,
                        // Body
                        content
                    ).and_then( 
                        |_a| Ok(format!("{}\n{}", cache,  guid))
                    )?;
                }
            }
        }
    }

    // TODO: Remove old guid from cache

    // save cache
    fs::write(cachefile, cache)?;


    Ok(())
}

